[[!meta title="How to use dep-team.pages.debian.net for your own proposals"]]

Using dep-team.pages.debian.net
===============================

You are welcome to maintain your DEP under the _dep-team.pages.debian.net_
namespace. This is attached to the _dep-team/deps_ Salsa project, to which all
developers have write access to, and it runs [ikiwiki][] through the GitLab
CI infrastructure.

  [ikiwiki]: http://ikiwiki.info

```
$ git clone git@salsa.debian.org:dep-team/deps.git
$ cd deps
# Record the new DEP in the index
$ editor web/index.md
# Add the content of the DEP
$ editor web/deps/depN.md
$ git add web
$ git commit -m 'Add DEP-N'
$ git push
```
(replace `N` with your DEP number)

The file is in Markdown format, which basically means that plain text
will format nicely, and for extra goodies you can consult [this brief
help][syntax1], or the full [Markdown syntax documentation][syntax2].
The only caveat is for the first paragraph: please indent all of its
lines with 4 spaces, so that it gets treated as &lt;pre>; see e.g.
[dep0.md][].

  [syntax1]: http://ikiwiki.info/ikiwiki/formatting/index.html
  [syntax2]: http://daringfireball.net/projects/markdown/syntax
  [dep0.md]: https://salsa.debian.org/dep-team/deps/-/blob/master/web/deps/dep0.md
