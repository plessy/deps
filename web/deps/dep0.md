[[!meta title="DEP-0: Introducing Debian Enhancement Proposals (DEPs)"]]

    Title: Introducing Debian Enhancement Proposals (DEPs)
    DEP: 0
    State: CANDIDATE
    Date: 2009-07-26
    Drivers: Stefano Zacchiroli <zack@debian.org>,
     Charles Plessy <plessy@debian.org>,
     Ben Finney <ben+debian@benfinney.id.au>
    URL: http://dep.debian.net/deps/dep0
    Source: https://salsa.debian.org/dep-team/deps/-/blob/master/web/deps/dep0.md
    License: http://www.jclark.com/xml/copying.txt
    Abstract:
     Workflow for managing discussions about improvements to Debian
     and archiving their outcomes.


Introduction
------------

This is a proposal to organize discussions about Debian enhancements,
reflect their current status and, in particular, to archive their
outcomes, via a new lightweight process based on Debian Enhancement
Proposals (DEPs). This idea is loosely based on existing similar systems
such as RFCs and Python PEPs. It is also completely opt-in, and does not
involve any new committees, powers, or authorities.


Motivation
----------

Currently, when having discussions about improvements to Debian, it is
not always clear when consensus has been reached, and people willing to
implement it may start too early, leading to wasted efforts, or delay it
indefinitely, because there's not clear indication it is time to begin. At the
same time, potential adopters of an enhancement may not be able to
easily assess whether they should use said implementation or not,
because it's difficult to know whether it adjusts to the consensus
reached during the discussion period.

Our normative documents rely on wide adoption of a practice before
documenting it, and adopters can be reluctant to make use of it before a
clear indication that a practice has some consensus behind it. This
creates a hard to break loop that this process hopes to alleviate, by
providing a mechanism to reflect the status of each proposal, including
whether it has reached consensus or not.

Secondly, we lack at the moment a central index in which to list such
proposals, which would be useful to see at a glance what open fronts
there are at a given moment in Debian, and who is taking care of them
and, additionally, to serve as a storage place for successfully
completed proposals, documenting the outcome of the discussion and the
details of the implementation.

By using this process, people involved in developing any enhancement can
help to build such index, with very little overhead required on their
part.


Workflow
--------

A "Debian enhancement" can be pretty much any change to Debian,
technical or otherwise. Examples of situations when the DEP process
might be or might have been used include:

* Introducing new debian/control fields (Homepage, Vcs-*).
* Making debian/copyright be machine parseable.
* Agreeing upon a meta-package name or virtual package name.
* Deciding on a procedure for the Debconf team for assigning travel
  sponsorship money.
* Formalizing existing informal processes or procedures, e.g.,
  the procedure for getting a new architecture added to the archive, or
  getting access to piatti.debian.org to run QA tests.

The workflow is very simple, and is intended to be quite lightweight:
an enhancement to Debian is suggested, discussed, implemented, and
becomes accepted practice (or policy, if applicable), in the normal
Debian way. As the discussion progresses, the enhancement is assigned
certain states, as explained below. During all the process, a single URL
maintained by the proposers can be used to check the status of the
proposal.

The result of all this is:

  1. an implementation of the enhancement and
  2. a document that can be referred to later on without having to dig
     up and read through large discussions.

The actual discussions should happen in the usual forum or forums for
the topic of the DEP. This way, DEPs do not act as yet another forum to
be followed. For example, a DEP suggesting changes to www.debian.org
graphical design should happen on debian-www, as usual.

In the same way, DEPs do not give any extra powers or authority to
anyone: they rely on reaching consensus in the traditional Debian way,
by engaging in discussions on mailing lists, IRC, or real life meetings
as appropriate, and not by consulting an external body for a decision.
To be acceptable, this consensus includes agreement from affected
parties, including those who would have to implement it or accept an
implementation.

The person or people who do the suggestion are the "drivers" of the
proposal and have the responsibility of writing the initial draft, and
of updating it during the discussions, see below.


Proposal states
---------------

<div style="float: right; text-align: center;">
[[!img deps/dep0/workflow.png size="500x250" alt="DEP state diagram"]]
<br />
<span style="font-size: xx-small">DEP workflow: state diagram</span>
</div>
A given DEP can be in one of the following *states*:

* DRAFT
* CANDIDATE
* ACCEPTED
* REJECTED
* OBSOLETE

The ideal progression of states is DRAFT -> CANDIDATE -> ACCEPTED, but
reality requires a couple of other states and transitions as well.

### DRAFT state: discussion

* every new proposal starts as a DRAFT
* anyone can propose a draft
* each draft has a number (next free one from document index)
* normal discussion and changes to the text happen in this state
* drafts should include *extra* criteria for success (in addition to
  having obtained consensus, see below), that is, requirements to
  finally become ACCEPTED

#### DRAFT -> CANDIDATE: rough consensus

In order for a DEP to become CANDIDATE, the following condition should
be met:

* consensus exists for *what* should be done, and *how* it should be
  done (agreement needs to be expressed by all affected parties, not
  just the drivers; silence is not agreement, but unanimity is not
  required, either)

### CANDIDATE: implementation + testing

The CANDIDATE state is meant to prove, via a suitable implementation
and its testing, that a given DEP is feasible.

* of course, implementation can start in earlier states
* changes to the text can happen also in this period, primarily based
  on feedback from implementation
* this period must be long enough that there is consensus that the
  enhancement works (on the basis of implementation evaluation)
* since DEP are not necessarily technical, "implementation" does not
  necessarily mean coding

#### CANDIDATE -> ACCEPTED: working implementation

In order for a DEP to become ACCEPTED, the following condition should
be met:

* consensus exists that the implementation has been a success

### ACCEPTED: have fun

Once accepted:

* the final version of the DEP text is archived on
  <http://dep.debian.net> for future reference
* if applicable, the proposed DEP change is integrated into
  authoritative texts such as policy, developer's reference, etc.

#### {DRAFT, CANDIDATE} -> REJECTED

A DEP can become REJECTED in the following cases:

* the drivers are no longer interested in pursuing the DEP and
  explicitly acknowledge so
* there are no modifications to a DEP in DRAFT state for 6 months or
  more
* there is no consensus either on the draft text or on the fact that
  the implementation is working satisfactorily

#### ACCEPTED -> OBSOLETE: no longer relevant

A DEP can become OBSOLETE when it is no longer relevant, for example:

* a new DEP gets accepted overriding previous DEPs (in that case the
  new DEP should refer to the one it OBSOLETE-s)
* the object of the enhancement is no longer in use

### {REJECTED, OBSOLETE}

In one of these states, no further actions are needed.

It is recommended that DEPs in one of these states carry a reason
describing why they have moved to such a state.


What the drivers should do
--------------------------

The only additional burden of the DEP process falls on the shoulders of its
drivers. They have to take care of all the practical work of writing
and maintaining the text, so that everyone else can just continue
discussing things as before.  Driver's burden can be summarized as:

* Write the draft text and update it during discussion.
* Determine when (rough) consensus in discussion has been reached.
* Implement, or find volunteers to implement.
* Determine when consensus of implementation success has been reached,
  when the testing of the available implementation has been satisfactory.
* Update the DEP with progress updates at suitable intervals, until the
  DEP has been accepted (or rejected).

If the drivers go missing in action, other people may step in and
courteously take over the driving position.

**Note**: the drivers can of course participate in the discussion as
everybody else, but have no special authority to impose their ideas to
others. <q>DEP gives pencils, not hammers.</q>


Format and content
------------------

A DEP is basically a free-form plain text file, except that it must
start with a paragraph of the following RFC822-style headers:

* Title: the full title of the document
* DEP: the number for this DEP
* State: the current state of this revision
* Date: the date of this revision
* Drivers: a list of drivers (names and e-mail addresses), in RFC822
  syntax for the To: header
* URL: during DRAFT state, a link to the canonical place of the draft
  (typically probably http://wiki.debian.org/DEP/DEPxxx or
  http://dep.debian.net/deps/depXXX)
* Source: one or more URLs to browse the sources used to produce the displayed
  version of the DEP.
* Abstract: a short paragraph (formatted as the long Description in
  debian/control)

(Additionally, REJECTED DEPs can carry a "Reason:" field describing
why they were rejected.)

The rest of the file is free form. If the DEP is kept in a wiki, using
its markup syntax is, of course, a good idea.

Suggested document contents:

* An introduction, giving an overview of the situation and the motivation
  for the DEP.
* A plan for implementation, especially indicating what parts of Debian need
  to be changed, and preferably indicating who will do the work.
* Preferably a list of criteria to judge whether the implementation has been
  a success.
* Links to mailing list threads, perhaps highlighting particularly important
  messages. If discussion happens on IRC, pointers to logs would be nice.


License
-------

The DEP must have a license that is DFSG free. You may choose the
license freely, but the "Expat" license is recommended. The
official URL for it is <http://www.jclark.com/xml/copying.txt> and
the license text is:

    Copyright (c) <year>  <your names>
    
    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

The justification for this recommendation is that this license is one
of the most permissive of the well-known licenses. By using this
license, it is easy to copy parts of the DEP to other places, such as
documentation for Debian development or embedded in individual
packages.



Creating a DEP
--------------

The procedure to create a DEP is simple: send an e-mail to
`debian-project@lists.debian.org`, stating that you're taking the next
available number, and including the first paragraph of the DEP as
explained above. It is very important to include the list of drivers,
and the URL where the draft will be kept up to date. The next available
DEP number can be obtained by consulting <http://dep.debian.net>.

It is also a very good idea to mention in this mail the place where the
discussion is going to take place, with a pointer to the thread in the
mailing list archives if it has already started.

Additionally, drivers are welcome to maintain their DEPs, even in the
draft state, in a repository inside the `dep` Alioth project, following
the instructions at <http://dep.debian.net/depdn-howto>. They are free not to
do so, and in that case a DEP0 driver or some interested party will
update the `dep.debian.net` index with their DEP, and a pointer to the
URL they provided.


Revising an accepted DEP
------------------------

If the feature, or whatever, of the DEP needs further changing later,
the process can start over with the accepted version of the DEP document
as the initial draft. The new draft will get a new DEP number. Once the
new DEP is accepted, the old one should move to OBSOLETE state.



License
-------

The following copyright statement and license apply to DEP0 (this
document).

Copyright (c) 2008-2009  Stefano Zacchiroli, Adeodato Simó, Lars Wirzenius

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Changes
-------

* 2013-12-25:
  [ Charles Plessy ]
  * DEP 0 drivers: removed Adeodato and Lars; added Ben and Charles.

* 2012-03-25:
  [ Charles Plessy ]
  * Added a "Source" field, to transfer the VCS information from the index to
    the DEP itself.

* 2009-07-26:
  [ Zack, Dato, Lars]
  * Re-word state description: describe separately states and
    transitions.
  * Rename "DROPPED" status to "REJECTED"; inhibit "resurrection" of
    REJECTED DEPs.
  * Mention that dep.debian.net is the central archival place
  * Stress that DEPs give no special powers to drivers, in the "driver
    role" section
  * Change the state of DEP0 to CANDIDATE

* 2008-06-12:
  [ Lars Wirzenius ]
  * Added a recommendation for the Expat license for new DEPs.
  * Set this DEP to be licensed under the Expat license.

* 2008-05-29:
  [ Lars Wirzenius ]
  * Added section saying that a DEP should have a DFSG free license.

* 2008-01-15:
  [ Adeodato Simó ]
  * Add section about how to create a DEP.
  * Rewrite "Introduction" (splitting "Motivation" off), and parts of
    "Workflow" as well.

  [ Lars Wirzenius ]
  * Typo fixes.

* 2008-01-11: Minor tweaks by Zack (mostly cosmetic, but also
  some more detailed specification of former more vague aspects)

* 2008-01-09: Various cleanups and tweaks by Lars, based on feedback
  from several parties.

* 2007-12-01: Initial version written after some quick brainstorming at
  the QA meeting in Extremadura, Spain, by Stefano, Adeodato, and Lars.

