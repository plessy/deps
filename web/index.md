[[!meta title="Debian Enhancement Proposals"]]

<div class="frontpage">
<h1>DEP - Debian Enhancement Proposals</h1>
</div>

*Debian Enhancement Proposals* (or *DEPs*, for short) offer a device to
**organize discussions** about various kinds of enhancements in the
[Debian project](http://www.debian.org), reflect their **current
status** and, in particular, **archive their outcomes**.

<div style="float: right; text-align: center;">
[[!img deps/dep0/workflow.png size="400x200" alt="DEP state diagram"]]
<br />
<span style="font-size: xx-small">DEP workflow: state diagram</span>
</div>
In essence, each enhancement proposals is assigned to one or more
**drivers** which follow the proposals advancing through various
[**states**](deps/dep0/workflow.png), until it is finally accepted (or
rejected). Discussions about ongoing DEPs happen in the usual Debian way
(i.e., the most appropriate mailing list, IRC channel, etc.).

More rationale and the full details about DEPs can be found in [**DEP 0:
Introducing Debian Enhancement Proposals**](deps/dep0).

DEP index
---------

| Status         | Title  |
| ---            | ---    |
| **CANDIDATE**  | [*DEP 0: Introducing Debian Enhancement Proposals*](deps/dep0) |
| **ACCEPTED**   | [*DEP 1: Clarifying policies and workflows for Non Maintainer Uploads (NMUs)*](deps/dep1.html) |
| **DRAFT**      | [*DEP 2: Debian Package Maintenance Hub*](deps/dep2) |
| **ACCEPTED**   | [*DEP 3: Patch Tagging Guidelines*](deps/dep3) |
| **DRAFT**      | [*DEP 4: Translation packages in Debian (TDebs)*](deps/dep4) |
| **ACCEPTED**   | [*DEP 5: Machine-readable debian/copyright*](deps/dep5) |
| **REJECTED**   | [*DEP 6: Meta-Package debian/control field*](deps/dep6) |
| **REJECTED**   | [*DEP 7: Java Web Application Packaging*](deps/dep7) |
| **DRAFT**      | [*DEP 8: automatic as-installed package testing*](deps/dep8) |
| **OBSOLETE**   | [*DEP 9: inet-superserver configuration by maintainer scripts*](deps/dep9) |
| **RESERVED**   | *DEP 10: parallelized ("rolling") release management* |
| **ACCEPTED**   | [*DEP 11: AppStream and Component Metadata for Debian*](deps/dep11) |
| **DRAFT**      | [*DEP 12: Per-package machine-readable metadata about Upstream*](deps/dep12) |
| **DRAFT**      | [*DEP 13: Debian specific homepage field*](deps/dep13) |
| **CANDIDATE**  | [*DEP 14: Recommended layout for Git packaging repositories*](deps/dep14) |
| **DRAFT**      | [*DEP 15: Reserved namespace for DD-approved non-maintainer changes*](deps/dep15) |
| **DRAFT**      | [*DEP 16: Improved confidential voting for General Resolutions*](deps/dep16) |


Additional information
----------------------

* You are free to use <https://salsa.debian.org/dep-team/deps> to work on DEPs, check the
  [[howto|depdn-howto]] for more information.  Please note that you should
  also read and follow the procedure outlined in [[DEP 0|deps/dep0]] 
  (especially with respect to announcing the DEP) before starting work
  on your DEP.
